package com.binder.hamburgueria.api.controller;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

import com.binder.hamburgueria.api.entity.Link;
import com.binder.hamburgueria.api.entity.User;
import com.binder.hamburgueria.api.service.LinkService;
import com.binder.hamburgueria.api.service.email.EmailService;
import com.binder.hamburgueria.api.service.email.Mail;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.binder.hamburgueria.api.response.Response;
import com.binder.hamburgueria.api.service.UserService;
import com.mongodb.DuplicateKeyException;

import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping("/api/user")
@CrossOrigin(origins = "*")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private LinkService linkService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @PostMapping()
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Response<User>> create(HttpServletRequest request, @RequestBody User user,
                                                 BindingResult result) {
        Response<User> response = new Response<User>();
        try {
            validateCreateUser(user, result);
            if (result.hasErrors()) {
                result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
                return ResponseEntity.badRequest().body(response);
            }
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            User userPersisted = (User) userService.createOrUpdate(user);
            response.setData(userPersisted);
        } catch (DuplicateKeyException dE) {
            response.getErrors().add("E-mail already registered !");
            return ResponseEntity.badRequest().body(response);
        } catch (Exception e) {
            response.getErrors().add(e.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
        return ResponseEntity.ok(response);
    }

    private void validateCreateUser(User user, BindingResult result) {
        if (user.getEmail() == null) {
            result.addError(new ObjectError("User", "Email no information"));
            return;
        }
    }

    @PutMapping()
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Response<User>> update(HttpServletRequest request, @RequestBody User user,
                                                 BindingResult result) {
        Response<User> response = new Response<User>();
        try {
            validateUpdate(user, result);
            if (result.hasErrors()) {
                result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
                return ResponseEntity.badRequest().body(response);
            }
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            User userPersisted = (User) userService.createOrUpdate(user);
            response.setData(userPersisted);
        } catch (Exception e) {
            response.getErrors().add(e.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
        return ResponseEntity.ok(response);
    }

    private void validateUpdate(User user, BindingResult result) {
        if (user.getId() == null) {
            result.addError(new ObjectError("User", "Id no information"));
            return;
        }
        if (user.getEmail() == null) {
            result.addError(new ObjectError("User", "Email no information"));
            return;
        }
    }

    @GetMapping(value = "{id}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Response<User>> findById(@PathVariable("id") String id) {
        Response<User> response = new Response<User>();
        User user = userService.findById(id);
        if (user == null) {
            response.getErrors().add("Register not found id:" + id);
            return ResponseEntity.badRequest().body(response);
        }
        response.setData(user);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Response<String>> delete(@PathVariable("id") String id) {
        Response<String> response = new Response<String>();
        User user = userService.findById(id);
        if (user == null) {
            response.getErrors().add("Register not found id:" + id);
            return ResponseEntity.badRequest().body(response);
        }
        userService.delete(id);
        return ResponseEntity.ok(new Response<String>());
    }

    @GetMapping(value = "{page}/{count}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Response<Page<User>>> findAll(@PathVariable int page, @PathVariable int count) {
        Response<Page<User>> response = new Response<Page<User>>();
        Page<User> users = userService.findAll(page, count);
        response.setData(users);
        return ResponseEntity.ok(response);
    }

    @GetMapping(value = "all")
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    public ResponseEntity<Response<List<User>>> findAll() {
        Response<List<User>> response = new Response<List<User>>();
        List<User> users = userService.findAll();
        response.setData(users);
        return ResponseEntity.ok(response);
    }



    @PostMapping("/solicitar-recuperacao-senha")
    public ResponseEntity<?> solicitarRecuperacaoSenha(@RequestBody() Map<String, Object> body) {

        User user = userService.findByEmail(body.get("email").toString());

        if (user == null) {

            return ResponseEntity.badRequest().body(new Response(null, "E-mail não cadastrado!!"));
        } else {


            Calendar now = Calendar.getInstance();
            Link link = new Link("http://localhost:4200/forgot-password/", user.getEmail(), new Date(now.getTimeInMillis()), false);

            Date validade = new Date(now.getTimeInMillis() + (60000 * 60));
            link.setValidade(validade);


            link = linkService.createOrUpdate(link);


            link.setLink(link.getLink() + link.getId());
            link = linkService.createOrUpdate(link);


            // Enviar o email
            Mail mail = new Mail();
            mail.setFrom("jteste4578@gmail.com");
            mail.setTo(user.getEmail());
            mail.setContent("");
            mail.setSubject("Recuperar Senha");


            mail.setTemplate(EmailService.EMAIL_RECUPERAR_SENHA);



            Map model = new HashMap();
            model.put("userName", user.getNome());
            model.put("link", link.getLink());

            mail.setModel(model);

            try {
                emailService.sendEmail(mail);
            } catch (MessagingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (TemplateException e) {
                e.printStackTrace();
            }
        }

        return ResponseEntity.ok(new Response(null, "Enviamos as instruções de recuperação de senha no seu e-mail!!"));
    }



    @PostMapping("/recuperar-senha")
    public ResponseEntity<?> recuperarSenha(@RequestBody() Map<String, Object> body) {

        // Buscar o link utilizado
        Link link = linkService.findById(body.get("linkId").toString());

        if (link.isUtilizado() || link.getValidade().before(new Date())) {
            return ResponseEntity.badRequest().body(new Response(null, "Link já utilizado ou expirado!"));
        }

        // Busca o usuário pelo e-mail
        User user = userService.findByEmail(link.getEmail());

        user.setPassword(passwordEncoder.encode(body.get("password").toString()));

        userService.createOrUpdate(user);

        link.setUtilizado(true);

        linkService.createOrUpdate(link);

        return ResponseEntity.ok(new Response(null, "Senha recuperada com sucessso"));
    }

    @GetMapping("validar-link/{linkId}")
    public ResponseEntity<?> validarLink(@PathVariable("linkId") String linkId) {
        Link link = linkService.findById(linkId);

        if (link.isUtilizado() || link.getValidade().before(new Date())) {
            return ResponseEntity.badRequest().body(new Response(null, "Link já utilizado ou expirado!"));
        }

        return ResponseEntity.ok(new Response(null, "Link OK"));
    }

}
