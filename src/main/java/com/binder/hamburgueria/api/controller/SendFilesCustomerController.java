package com.binder.hamburgueria.api.controller;


import com.binder.hamburgueria.api.entity.SendFilesCustomer;
import com.binder.hamburgueria.api.entity.User;
import com.binder.hamburgueria.api.response.Response;
import com.binder.hamburgueria.api.security.jwt.JwtTokenUtil;
import com.binder.hamburgueria.api.service.FileService;
import com.binder.hamburgueria.api.service.SendFilesCustomerService;
import com.binder.hamburgueria.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

@RestController
@RequestMapping("/api/sendFilesCustomer")
@CrossOrigin(origins = "*")
public class SendFilesCustomerController {

    private DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    private DateFormat df2 = new SimpleDateFormat("MM-yyyy");

    @Autowired
    private SendFilesCustomerService sendFilesCustomerService;

    @Autowired
    protected JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserService userService;

    @Autowired
    private FileService fileService;


    @PostMapping()
    @PreAuthorize("hasAnyRole('CUSTOMER', 'ADMIN')")
    public ResponseEntity<Response<SendFilesCustomer>> update(HttpServletRequest request, @RequestBody SendFilesCustomer sendFilesCustomer,
                                                              BindingResult result) {
        Response<SendFilesCustomer> response = new Response<SendFilesCustomer>();
        try {
            validateCreateSendFile(sendFilesCustomer, result);
            if (result.hasErrors()) {
                result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
                return ResponseEntity.badRequest().body(response);
            }

            // Pedido Novo
            if(sendFilesCustomer.getId() == null){
                sendFilesCustomer.setUser(userFromRequest(request));
                sendFilesCustomer.setDate(df.format(new Date()));
                sendFilesCustomer.setMesAno(df2.format(new Date()));
                sendFilesCustomer.setNumber(generateNumber());
            }


            SendFilesCustomer sendFilesCustomerPersisted = (SendFilesCustomer) sendFilesCustomerService.update(sendFilesCustomer);
            response.setData(sendFilesCustomerPersisted);
        } catch (Exception e) {
            response.getErrors().add(e.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
        return ResponseEntity.ok(response);
    }


    @GetMapping(value = "{mesAno}")
    //@PreAuthorize("hasAnyRole('CUSTOMER','TECHNICIAN')")
    public ResponseEntity<Response<SendFilesCustomer>> findByMesAno(@PathVariable("mesAno") String mesAno) {
        return ResponseEntity.ok(new Response(sendFilesCustomerService.findByMesAno(mesAno), "Consulta realizada com sucess!!"));
    }

    @GetMapping(value = "list-all")
    @PreAuthorize("hasAnyRole('CUSTOMER','TECHNICIAN')")
    public ResponseEntity<Response<SendFilesCustomer>> findAll(HttpServletRequest request) {
        User user = userFromRequest(request);
        return ResponseEntity.ok(new Response(sendFilesCustomerService.findByUser_Id(user.getId()), "Consulta realizada com sucess!!"));
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','CUSTOMER', 'ADMIN')")
    public ResponseEntity<Response<String>> delete(@PathVariable("id") String id) {
        Response<String> response = new Response<String>();
        sendFilesCustomerService.delete(id);
        return ResponseEntity.ok(new Response(null, "Arquio deletado com sucesso!"));
    }

    @GetMapping(value = "file/{id}", produces = MediaType.ALL_VALUE)
    @PreAuthorize("hasAnyRole('ADMIN','CUSTOMER','TECHNICIAN')")
    public ResponseEntity<InputStreamResource> findFileById(@PathVariable("id") String id) throws Exception {
        SendFilesCustomer ticket = sendFilesCustomerService.findById(id);
        File result = fileService.base64ToFile(ticket.getFile());

        try {
            return ResponseEntity
                    .ok()
                    .body(new InputStreamResource(new FileInputStream(result)));
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("O recurso solicitado não está mais disponível ou foi movido!");
        }
    }


    private void validateCreateSendFile(SendFilesCustomer sendFilesCustomer, BindingResult result) {
        if (sendFilesCustomer.getSubject() == null) {
            result.addError(new ObjectError("Subject", "Subject no information"));
            return;
        }
    }

    public User userFromRequest(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        String email = jwtTokenUtil.getUsernameFromToken(token);
        return userService.findByEmail(email);
    }

    private Integer generateNumber() {
        Random random = new Random();
        return random.nextInt(9999);
    }


}
