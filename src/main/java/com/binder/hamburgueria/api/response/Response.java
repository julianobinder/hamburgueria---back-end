package com.binder.hamburgueria.api.response;

import java.util.ArrayList;
import java.util.List;

//todos os métodos retornam essa classe
public class Response<T>{

	private T data;
	private List<String> errors;
	private String message;


	public Response( T data, String message){
		this.data = data;
		this.message = message;
	}

	public Response(){

	}


	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public List<String> getErrors() {
		if(this.errors == null) {
			this.errors = new ArrayList<String>();
		}
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
