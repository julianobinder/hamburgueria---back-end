package com.binder.hamburgueria.api.repository;

import com.binder.hamburgueria.api.entity.Ticket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TicketRepository extends MongoRepository<Ticket, String> {


    Page<Ticket> findByUserIdOrderByDateDesc(Pageable pages, String userId);


    Page<Ticket> findByNumber(Integer number, Pageable pages);


    Page<Ticket> findByTitleIgnoreCaseContainingAndStatusIgnoreCaseContainingAndPriorityIgnoreCaseContainingAndMesAnoIgnoreCaseContainingOrderByDateDesc(
            String title,String status,String priority,String mesAno, Pageable pages);


    Page<Ticket> findByTitleIgnoreCaseContainingAndStatusIgnoreCaseContainingAndPriorityIgnoreCaseContainingAndUserIdOrderByDateDesc(
            String title,String status,String priority,String userId,Pageable pages);


    Page<Ticket> findByTitleIgnoreCaseContainingAndStatusIgnoreCaseContainingAndPriorityIgnoreCaseContainingAndAssignedUserIdOrderByDateDesc(
            String title,String status,String priority,String assignedUserId,Pageable pages);


    List<Ticket> findByDateAndTipoAndCustomerId(String date, String tipo, String userId);


    List<Ticket> findByMesAnoAndTipoAndCustomerId(String anomes, String tipo, String userId);
}
