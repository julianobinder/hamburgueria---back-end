package com.binder.hamburgueria.api.repository;

import com.binder.hamburgueria.api.entity.Link;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LinkRepository extends MongoRepository<Link, String> {


	Link findById(String id);

}
