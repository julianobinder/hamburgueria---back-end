package com.binder.hamburgueria.api.repository;

import com.binder.hamburgueria.api.entity.SendFilesCustomer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface SendFilesCustomerRepository extends MongoRepository<SendFilesCustomer,String> {
    List<SendFilesCustomer> findByMesAno(String mesAno);

    List<SendFilesCustomer> findByUser_Id(String id);


    SendFilesCustomer findById(String id);
}
