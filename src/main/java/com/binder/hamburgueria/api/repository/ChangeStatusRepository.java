package com.binder.hamburgueria.api.repository;

import com.binder.hamburgueria.api.entity.ChangeStatus;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ChangeStatusRepository extends MongoRepository<ChangeStatus, String> {

	//buscar todas alterações daquele ticket
	Iterable<ChangeStatus> findByTicketIdOrderByDateChangedStatusDesc(String ticketId);
		
	
}
