package com.binder.hamburgueria.api.repository;

import com.binder.hamburgueria.api.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

	User findByEmail(String email);

}
