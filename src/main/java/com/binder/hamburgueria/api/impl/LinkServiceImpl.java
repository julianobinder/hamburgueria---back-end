package com.binder.hamburgueria.api.impl;

import com.binder.hamburgueria.api.entity.Link;
import com.binder.hamburgueria.api.repository.LinkRepository;
import com.binder.hamburgueria.api.service.LinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LinkServiceImpl implements LinkService {

	@Autowired
	private LinkRepository linkRepository;

    @Override
    public Link createOrUpdate(Link link) {

        return linkRepository.save(link);
    }

    @Override
    public Link findById(String id) {

        return linkRepository.findById(id);
    }
}
