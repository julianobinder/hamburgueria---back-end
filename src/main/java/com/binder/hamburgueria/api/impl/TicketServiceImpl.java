package com.binder.hamburgueria.api.impl;

import com.binder.hamburgueria.api.entity.ChangeStatus;
import com.binder.hamburgueria.api.entity.Ticket;
import com.binder.hamburgueria.api.repository.ChangeStatusRepository;
import com.binder.hamburgueria.api.repository.TicketRepository;
import com.binder.hamburgueria.api.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TicketServiceImpl implements TicketService {

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private ChangeStatusRepository changeStatusRepository;

    @Override
    public Ticket createOrUpdate(Ticket ticket) {

        return this.ticketRepository.save(ticket);
    }

    @Override
    public Ticket findById(String id) {
        return this.ticketRepository.findOne(id);
    }

    @Override
    public void delete(String id) {
        this.ticketRepository.delete(id);
    }

    public Page<Ticket> listTicket(int page, int count) {
        Pageable pages = new PageRequest(page, count);
        return this.ticketRepository.findAll(pages);
    }

    @Override
    public ChangeStatus createChangeStatus(ChangeStatus changeStatus) {
        return this.changeStatusRepository.save(changeStatus);
    }


    @Override
    public Iterable<ChangeStatus> listChangeStatus(String ticketId) {
        return this.changeStatusRepository.findByTicketIdOrderByDateChangedStatusDesc(ticketId);
    }

    @Override
    public Page<Ticket> findByCurrentUser(int page, int count, String userId) {
        Pageable pages = new PageRequest(page, count);
        return this.ticketRepository.findByUserIdOrderByDateDesc(pages, userId);
    }

    public Page<Ticket> findByParameters(int page, int count, String title, String status, String priority, String anoMes) {
        Pageable pages = new PageRequest(page, count);
        return this.ticketRepository.
                findByTitleIgnoreCaseContainingAndStatusIgnoreCaseContainingAndPriorityIgnoreCaseContainingAndMesAnoIgnoreCaseContainingOrderByDateDesc(title, status, priority,anoMes, pages);
    }

    public Page<Ticket> findByParametersAndCurrentUser(int page, int count,String title,String status,
                                                       String priority,String userId) {
        Pageable pages = new PageRequest(page, count);
        return this.ticketRepository.
                findByTitleIgnoreCaseContainingAndStatusIgnoreCaseContainingAndPriorityIgnoreCaseContainingAndUserIdOrderByDateDesc(
                        title,status,priority,userId,pages);
    }

    public Page<Ticket> findByNumber(int page, int count,Integer number){
        Pageable pages = new PageRequest(page, count);
        return this.ticketRepository.findByNumber(number, pages);
    }

    @Override
    public List<Ticket> findAll() {
        return this.ticketRepository.findAll();
    }


    public Page<Ticket> findByParametersAndAssignedUser(int page, int count, String title, String status,
                                                        String priority, String assignedUserId) {
        Pageable pages = new PageRequest(page, count);
        return this.ticketRepository.
                findByTitleIgnoreCaseContainingAndStatusIgnoreCaseContainingAndPriorityIgnoreCaseContainingAndAssignedUserIdOrderByDateDesc(
                        title, status, priority, assignedUserId, pages);
    }

    @Override
    public List<Ticket> findByDateAndTipoAndCustomerId(String date, String tipo, String userId) {
        return this.ticketRepository.findByDateAndTipoAndCustomerId(date, tipo, userId);
    }

    @Override
    public List<Ticket> findByMesAnoAndTipoAndCustomerId(String anomes, String tipo, String userId) {
        return this.ticketRepository.findByMesAnoAndTipoAndCustomerId(anomes, tipo, userId);
    }


}
