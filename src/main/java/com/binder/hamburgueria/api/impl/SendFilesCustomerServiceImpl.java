package com.binder.hamburgueria.api.impl;

import com.binder.hamburgueria.api.entity.SendFilesCustomer;
import com.binder.hamburgueria.api.repository.SendFilesCustomerRepository;
import com.binder.hamburgueria.api.service.SendFilesCustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SendFilesCustomerServiceImpl implements SendFilesCustomerService {


    @Autowired
    private SendFilesCustomerRepository sendFilesCustomerRepository;


    @Override
    public SendFilesCustomer update(SendFilesCustomer sendFilesCustomer) {
        return sendFilesCustomerRepository.save(sendFilesCustomer);
    }

    @Override
    public List<SendFilesCustomer> findByMesAno(String mesAno) {
        return sendFilesCustomerRepository.findByMesAno(mesAno);
    }

    @Override
    public List<SendFilesCustomer> findByUser_Id(String id) {
        return sendFilesCustomerRepository.findByUser_Id(id);
    }

    @Override
    public void delete(String id) {
        sendFilesCustomerRepository.delete(id);
    }

    @Override
    public SendFilesCustomer findById(String id) {
        return sendFilesCustomerRepository.findById(id);
    }
}

