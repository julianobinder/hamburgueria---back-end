package com.binder.hamburgueria.api.service;

import com.binder.hamburgueria.api.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface UserService {

	User findByEmail(String email);
	
	User createOrUpdate(User user);
	
	User findById(String id);
	
	void delete (String id);
 	
    Page<User> findAll(int page, int count );

	List<User> findAll();

}
