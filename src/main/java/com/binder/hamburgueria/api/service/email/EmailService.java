/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binder.hamburgueria.api.service.email;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;


@Service
public class EmailService {

    public static final String EMAIL_RECUPERAR_SENHA = "recuperar-senha/index";

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private FreeMarkerConfigurer freemarkerConfig;

    public void sendEmail(Mail mail) throws MessagingException, IOException, TemplateException {
        send(mail);
    }

    private void send(Mail mail) throws MessagingException, IOException, TemplateException {
        MimeMessage message = emailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());

        Template t;
        String html;

        if (mail.getTemplate() != null) {
            t = freemarkerConfig.getConfiguration().getTemplate(mail.getTemplate() + ".ftl");
            html = FreeMarkerTemplateUtils.processTemplateIntoString(t, mail.getModel());
            helper.setText(html, true);
        } else {
            helper.setText(mail.getText());
        }

        helper.setTo(mail.getTo());
        helper.setSubject(mail.getSubject());
        helper.setFrom(mail.getFrom());

        emailSender.send(message);
    }


}
