package com.binder.hamburgueria.api.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Service;

@Service
public class FileService {
	

	public File base64ToFile(final String b64) {
		try {
			
			String formato = b64.substring(5, 14);
			
			if(formato.equals("image")){
				formato = formato.split("/")[1];
			}else {
				formato = b64.substring(5, 20).split("/")[1];
			}
			
            File result = File.createTempFile(""+new Date().getTime(), "." + b64.substring(5, 20).split("/")[1]);
            byte[] data = Base64.getDecoder().decode(b64.split(",")[1]);
            try (OutputStream stream = new FileOutputStream(result)) {
                stream.write(data);
            }
            return result;
        } catch (IOException ex) {
            Logger.getLogger(FileService.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
	}

}
