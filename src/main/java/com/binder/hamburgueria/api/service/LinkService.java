package com.binder.hamburgueria.api.service;

import com.binder.hamburgueria.api.entity.Link;
import org.springframework.stereotype.Component;

@Component
public interface LinkService {
	Link createOrUpdate(Link user);
	Link findById(String id);
}
