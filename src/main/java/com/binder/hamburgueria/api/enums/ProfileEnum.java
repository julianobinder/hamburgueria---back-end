package com.binder.hamburgueria.api.enums;

public enum ProfileEnum {

	ROLE_ADMIN,
	ROLE_CUSTOMER,
	ROLE_TECHNICIAN
}
