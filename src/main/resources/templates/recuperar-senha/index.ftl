<div style="width:95%;display:block;margin:0 auto;background-color:#fff">
    <div class="adM">
    </div>
'

    <div style="padding:20px 40px">Olá ${userName},<br>
        <br>
        Aparentemente, você pediu para alterar sua senha. Para fazer isso basta clicar no botão abaixo!!
        <div style="color: #ffffff; background-color: #a8bf6f; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; max-width: 202px; width: 172px;width: auto; border-top: 0px solid transparent; border-right: 0px solid transparent; border-bottom: 0px solid transparent; border-left: 0px solid transparent; padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px; font-family: 'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif; text-align: center; mso-border-alt: none;">
            <a href="${link}" style="font-size:16px;line-height:32px;">ALTERAR SENHA</a>
        </div>
        <br>
        <br>
        Se não foi você, basta ignorar esse e-mail.
        <br>
        <br>
        Bons estudos!
    </div>

</div>


